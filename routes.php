<?php

use Rubium\ShopaholicSber\Classes\Helper\SberPaymentGateway;

App::before(function ($request) {

    /*
     * Defines the entry point to be used by the Socialite Plugin
     */
    Route::get('shopaholic/payment/sber/callback', [
        'as' => 'ShopaholicSberCallback',
        'uses' => 'Rubium\ShopaholicSber\Http\Controllers\AuthController@execute',
        'middleware' => 'web',
    ]);
});

Route::get(SberPaymentGateway::SUCCESS_RETURN_URL.'{slug}', function ($sSecretKey) {
    $obPaymentGateway = new SberPaymentGateway();
    return $obPaymentGateway->processSuccessRequest($sSecretKey);
});

Route::get(SberPaymentGateway::CANCEL_RETURN_URL.'{slug}', function ($sSecretKey) {
    $obPaymentGateway = new SberPaymentGateway();
    return $obPaymentGateway->processCancelRequest($sSecretKey);
});
