<?php namespace Rubium\ShopaholicSber;

use Backend, Event;
use Rubium\ShopaholicSber\Classes\Helper\SberPaymentGateway;
use System\Classes\PluginBase;

use Rubium\ShopaholicSber\Classes\Event\PaymentGatewayHandler;

/**
 * Class Plugin
 * @package Rubium\ShopaholicSber
 * @author Rubium
 */
class Plugin extends PluginBase
{
    /** @var array Plugin dependencies */
	public $require = ['Lovata.OrdersShopaholic', 'Lovata.Shopaholic', 'Lovata.Toolbox'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'ProductClone',
            'description' => 'No description provided yet...',
            'author'      => 'Rubium',
            'icon'        => 'icon-leaf'
        ];
    }
    
    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function boot()
    {
        $this->addListeners();
        Event::listen(SberPaymentGateway::EVENT_PROCESS_CANCEL_URL, function ($data){
//            dd($data);
//            TODO: change order status
        });
        Event::listen(SberPaymentGateway::EVENT_PROCESS_RETURN_URL, function ($data){
//            dd($data);
//            TODO: change order status
        });

        Event::listen(SberPaymentGateway::EVENT_GET_PAYMENT_GATEWAY_CANCEL_URL, function (){
            return url('payment/sber/fail');
        });
        Event::listen(SberPaymentGateway::EVENT_GET_PAYMENT_GATEWAY_RETURN_URL, function (){
            return url('payment/sber/success');
        });
    }

    private function addListeners()
    {
        Event::subscribe(PaymentGatewayHandler::class);
    }
}
