<?php namespace Rubium\ShopaholicSber\Classes;

use Voronkovich\SberbankAcquiring\Client;
use Voronkovich\SberbankAcquiring\Currency;
use Voronkovich\SberbankAcquiring\HttpClient\HttpClientInterface;
use Voronkovich\SberbankAcquiring\OrderStatus;

use Rubium\ShopaholicSber\Classes\Message\Response;

/**
 * Class Gateway
 * @package Rubium\ShopaholicSber\Classes
 * @author  Rubium
 */
class Gateway
{
    /**
     * Define gateway parameters, in the following format:
     *
     * array(
     *     'username' => '', // string variable
     *     'password' => '', // string variable
     *     'testMode' => false, // boolean variable
     * );
     */
    public function getDefaultParameters()
    {
        return [
            'userName' => '',
            'password' => '',
            'testMode' => false,
            'scheme' => [
                'One stage',
                'Two stage',
                /*in future*/
                // 'Apple Pay',
                // 'Google Pay',
                // 'Samsung Pay'
                /*in future*/
            ],
            'token' => '',
            'use_token' => false,
        ];
    }

    /**
     * Request for order registration
     *
     * @param array $options array of options
     * @return RequestInterface
     */
    public function purchase(array $options = [])
    {
        $user_data = [
            'currency' => constant(Currency::class . '::' . $options['currency']),
            'apiUri'   => Client::API_URI_TEST,
            'httpMethod' => HttpClientInterface::METHOD_GET,
            'language' => 'ru'
        ];
        if (!$options['testMode'])
            $user_data['apiUri'] = Client::API_URI;

        if($options['use_token'] && !empty($options['token'])){
            $user_data['token'] = $options['token'];
        }else{
            $user_data['userName'] = $options['userName'];
            $user_data['password'] = $options['password'];
        }

        $this->client = new Client($user_data);

        return $this->send($options);
    }

    public function send($options)
    {
        $params = array();

        $orderId = $options['description'];
        $orderAmount = $options['amount']*100;
        $returnUrl = $options['returnUrl'];

        $params['failUrl'] = $options['cancelUrl'];

        switch ($options['scheme']) {
            case 'One stage':
                $result = $this->client->registerOrder($orderId, $orderAmount, $returnUrl, $params);
                break;
            case 'Two stage':
                $result = $this->client->registerOrderPreAuth($orderId, $orderAmount, $returnUrl, $params);
                break;
            /*in future*/
            // case 'Apple Pay':
            //     $result = $this->client->payWithApplePay($orderId, $merchant, $paymentToken);
            //     break;
            // case 'Google Pay':
            //     $result = $this->client->payWithGooglePay($orderId, $merchant, $paymentToken);
            //     break;
            // case 'Samsung Pay':
            //     $result = $this->client->payWithSamsungPay($orderId, $merchant, $paymentToken);
            //     break;
            /*in future*/
            default:
                $result = $this->client->registerOrder($orderId, $orderAmount, $returnUrl, $params);
                break;
        }

        return $result;
    }

    public function orderStatus($orderId)
    {
        $result = $this->client->getOrderStatus($orderId);
        return $result;
        if (OrderStatus::isDeposited($result['orderStatus'])) {
            echo "Order #$orderId is deposited!";
        }

        if (OrderStatus::isDeclined($result['orderStatus'])) {
            echo "Order #$orderId was declined!";
        }
    }

    public function orderReverse()
    {
        $result = $this->client->reverseOrder($orderId, $amountToRefund);
    }

    public function orderRefund()
    {
        $result = $this->client->refundOrder($orderId, $amountToRefund);
    }

}
