<?php namespace Rubium\ShopaholicSber\Classes\Event;

use Event;

use Rubium\ShopaholicSber\Classes\Helper\SberPaymentGateway;
use Rubium\ShopaholicSber\Classes\Gateway;

use Lovata\OrdersShopaholic\Models\PaymentMethod;
use Lovata\OrdersShopaholic\Controllers\PaymentMethods;

/**
 * Class PaymentGatewayHandler
 * @package Rubium\ShopaholicSber\Classes\Event
 * @author Rubium
 */
class PaymentGatewayHandler
{
    const GATEWAY_CODE = 'Sberbank';

	/**
     * Add listeners
     * @param \Illuminate\Events\Dispatcher $obEvent
     */
    public function subscribe($obEvent)
    {


        $obEvent->listen(PaymentMethod::EVENT_GET_GATEWAY_LIST, function () {
            $arResult[self::GATEWAY_CODE] = self::GATEWAY_CODE;
            return $arResult;
        });

        $obEvent->listen(SberPaymentGateway::EVENT_GET_PAYMENT_GATEWAY_CLASS, function($sGatewayCode) {
            if ($sGatewayCode == SberPaymentGateway::CODE) {
                return SberPaymentGateway::class;
            }
            return null;
        });

        PaymentMethod::extend(function ($obElement) {
            /** @var PaymentMethod $obElement */

            $obElement->addGatewayClass(self::GATEWAY_CODE, SberPaymentGateway::class);
        });

        Event::listen('backend.form.extendFields', function ($obWidget) {

            // Only for the Settings controller
            if (!$obWidget->getController() instanceof PaymentMethods || $obWidget->isNested) {
                return;
            }

            // Only for the Settings model
            if (!$obWidget->model instanceof PaymentMethod || empty($obWidget->model->gateway_id)) {
                return;
            }
            //Create gateway object
            $obGateway = new Gateway;

            $arPropertyList = $obGateway->getDefaultParameters();

            //Process property list  for gateway
            foreach ($arPropertyList as $sPropertyName => $arValueList) {
                if (empty($sPropertyName)) {
                    continue;
                }

                if (is_array($arValueList)) {
                    $obWidget->addTabFields([
                        'gateway_property['.$sPropertyName.']' => [
                            'label'   => $sPropertyName,
                            'tab'     => 'lovata.ordersshopaholic::lang.tab.gateway',
                            'type'    => 'dropdown',
                            'span'    => 'left',
                            'options' => $this->prepareValueList($arValueList),
                        ],
                    ]);
                } elseif (is_bool($arValueList)) {
                    $obWidget->addTabFields([
                        'gateway_property['.$sPropertyName.']' => [
                            'label'   => $sPropertyName,
                            'tab'     => 'lovata.ordersshopaholic::lang.tab.gateway',
                            'type'    => 'checkbox',
                            'default' => $arValueList,
                            'span'    => 'left',
                        ],
                    ]);
                } else {
                    $obWidget->addTabFields([
                        'gateway_property['.$sPropertyName.']' => [
                            'label' => $sPropertyName,
                            'tab'   => 'lovata.ordersshopaholic::lang.tab.gateway',
                            'type'  => 'text',
                            'span'  => 'left',
                        ],
                    ]);
                }
            }
        });
	}

    /**
     * Prepare value list for backend field
     * @param array $arValueList
     * @return array
     */
    protected function prepareValueList($arValueList)
    {
        if (empty($arValueList) || !is_array($arValueList)) {
            return [];
        }

        if ($arValueList !== array_values($arValueList)) {
            return $arValueList;
        }

        $arResult = [];
        foreach ($arValueList as $sValue) {
            $arResult[$sValue] = $sValue;
        }
        return $arResult;
    }
}