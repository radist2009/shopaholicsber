<?php namespace Rubium\ShopaholicSber\Classes\Helper;

use Input;
use Event;
use Redirect;
use Validator;
use Lovata\OrdersShopaholic\Classes\Helper\AbstractPaymentGateway;
use Rubium\ShopaholicSber\Classes\Gateway;

/**
 * Class SberPaymentGateway
 * @package Rubium\ShopaholicSber\Classes\Helper
 * @author  Rubium
 */
class SberPaymentGateway extends AbstractPaymentGateway
{
    const CODE = 'Sberbank';

    
    const EVENT_GET_PAYMENT_GATEWAY_CLASS = 'shopaholic.payment_method.omnipay.gateway.class';
    const EVENT_GET_PAYMENT_GATEWAY_CANCEL_URL = 'shopaholic.payment_method.omnipay.gateway.cancel_url';
    const EVENT_GET_PAYMENT_GATEWAY_RETURN_URL = 'shopaholic.payment_method.omnipay.gateway.return_url';
    const EVENT_GET_PAYMENT_GATEWAY_PURCHASE_DATA = 'shopaholic.payment_method.omnipay.gateway.purchase_data';
    const EVENT_GET_PAYMENT_GATEWAY_CARD_DATA = 'shopaholic.payment_method.omnipay.gateway.card_data';
    
    const SUCCESS_RETURN_URL = 'payment/sber/success/';
    const CANCEL_RETURN_URL = 'payment/sber/cancel/';
    const NOTIFY_RETURN_URL = 'payment/sber/notice/';

    const STATUS_SUCCESS = 'PaymentActionCompleted';
    const STATUS_IN_PROGRESS = 'PaymentActionInProgress';
    const STATUS_FAILED = 'PaymentActionFailed';
    const STATUS_CANCEL = 'PaymentActionNotInitiated';

    const EVENT_PROCESS_RETURN_URL = 'shopaholic.payment_method.omnipay.gateway.process_return_url';
    const EVENT_PROCESS_CANCEL_URL = 'shopaholic.payment_method.omnipay.gateway.process_cancel_url';
    const EVENT_PROCESS_NOTIFY_URL = 'shopaholic.payment_method.omnipay.gateway.process_notify_url';

    protected $arCardData = [];
	protected $obGateway;

    /**
     * Send "fetch checkout" request and update transaction data
     */
    protected function updateTransactionData()
    {
        $this->sendFetchTransactionData();
        $this->processFetchTransactionResponse();
    }

    /**
     * Send fetchTransaction request to payment gateway
     */
    protected function sendFetchTransactionData()
    {
        try {
            $this->response = $this->obGateway->fetchTransaction($this->arPurchaseData)->send();
        } catch (\Exception $obException) {
            $this->sResponseMessage = $obException->getMessage();
            return;
        }
    }
    
    /**
     * Process fetchTransaction request to payment gateway
     */
    protected function processFetchTransactionResponse()
    {
        if (empty($this->response)) {
            return;
        }

        $arPaymentResponse = (array) $this->obOrder->payment_response;
        $arPaymentResponse['transaction'] = (array) $this->response->getData();

        $this->obOrder->payment_response = $arPaymentResponse;
    }

    /**
     * Get response array
     * @return array
     */
    public function getResponse() : array
    {
        return [];
    }

    /**
     * Get redirect URL
     * @return string
     */
    public function getRedirectURL() : string
    {
        if (empty($this->response)) {
            return '';
        }

        return (string) $this->response['formUrl'];
    }

    /**
     * Get response message
     * @return string
     */
    public function getMessage() : string
    {
        if (empty($this->response)) {
            return (string) $this->sResponseMessage;
        }
        
        return (string) $this->response->getMessage();

    }

    /**
     * Process success request
     * @param string $sSecretKey
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processSuccessRequest($sSecretKey)
    {
        $this->initOrderObject($sSecretKey);
        if (empty($this->obOrder) || empty($this->obPaymentMethod)) {
            return Redirect::to('/');
        }

        //Set success status in order
        $this->setSuccessStatus();

        Event::fire(self::EVENT_PROCESS_RETURN_URL, [
            $this->obOrder,
            $this->obPaymentMethod,
        ]);

        //Get redirect URL
        $sRedirectURL = $this->getReturnURL();

        return Redirect::to($sRedirectURL);
    }

    /**
     * Process cancel request
     * @param string $sSecretKey
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processCancelRequest($sSecretKey)
    {
        //Init order object
        $this->initOrderObject($sSecretKey);
        if (empty($this->obOrder) || empty($this->obPaymentMethod)) {
            return Redirect::to('/');
        }

        //Set cancel status in order
        $this->setCancelStatus();

        //Fire event
        Event::fire(self::EVENT_PROCESS_CANCEL_URL, [
            $this->obOrder,
            $this->obPaymentMethod,
        ]);

        //Get redirect URL
        $sRedirectURL = $this->getCancelURL();
        return Redirect::to($sRedirectURL);
    }

    /**
     * Prepare purchase data array for request
     * @return void
     */
    protected function preparePurchaseData()
    {
        if (empty($this->obOrder) || empty($this->obPaymentMethod) || empty($this->obPaymentMethod->gateway_id)) {
            return;
        }
        $this->obGateway = new Gateway;

        $this->arPurchaseData = [
            'amount'        => $this->obOrder->total_price_value,
            'currency'      => $this->obPaymentMethod->gateway_currency,
            'description'   => $this->obOrder->order_number,
            'transactionId' => $this->obOrder->transaction_id,
            'token'         => $this->obOrder->payment_token,
            'returnUrl'     => url(self::SUCCESS_RETURN_URL.$this->obOrder->secret_key),
            'cancelUrl'     => url(self::CANCEL_RETURN_URL.$this->obOrder->secret_key),
        ];

        //Get default property list for gateway
        $arPropertyList = $this->obGateway->getDefaultParameters();
        
        if (empty($arPropertyList)) {
            return;
        }

        foreach ($arPropertyList as $sFieldName => $sValue) {
            $this->arPurchaseData[$sFieldName] = $this->getGatewayProperty($sFieldName);
        }
        $this->extendPurchaseData();

        $this->arPurchaseData['returnUrl'] = url(self::SUCCESS_RETURN_URL.$this->obOrder->secret_key);
        $this->arPurchaseData['cancelUrl'] = url(self::CANCEL_RETURN_URL.$this->obOrder->secret_key);
        $this->arPurchaseData['notifyUrl'] = url(self::NOTIFY_RETURN_URL.$this->obOrder->secret_key);

    }

    /**
     * Get cancel URL
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    protected function getCancelURL()
    {
        return $this->getRedirectURLForPaymentGateway(self::EVENT_GET_PAYMENT_GATEWAY_CANCEL_URL);
    }

    /**
     * Get return URL
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    protected function getReturnURL()
    {
        return $this->getRedirectURLForPaymentGateway(self::EVENT_GET_PAYMENT_GATEWAY_RETURN_URL);
    }

    /**
     * Get redirect URL for purchase request params
     * @param string $sEventName
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    protected function getRedirectURLForPaymentGateway($sEventName)
    {
        //Fire event
        $arEventDataList = Event::fire($sEventName, [
            $this->obOrder,
            $this->obPaymentMethod,
        ]);
        if (empty($arEventDataList)) {
            return url('/');
        }

        //Process event data
        foreach ($arEventDataList as $sURL) {
            if (!empty($sURL) && is_string($sURL)) {
                return $sURL;
            }
        }

        return url('/');
    }

    /**
     * Fire event and extend purchase data
     */
    protected function extendPurchaseData()
    {
        //Fire event
        $arEventDataList = Event::fire(self::EVENT_GET_PAYMENT_GATEWAY_PURCHASE_DATA, [
            $this->obOrder,
            $this->obPaymentMethod,
            $this->arPurchaseData,
        ]);
        $arEventDataList = $this->arPurchaseData;

        if (empty($arEventDataList)) {
            return;
        }

        //Process event data
        foreach ($arEventDataList as $arEventData) {
            if (empty($arEventData) || !is_array($arEventData)) {
                continue;
            }

            foreach ($arEventData as $sField => $sValue) {
                $this->arPurchaseData[$sField] = $sValue;
            }
        }
    }

    /**
     * Validate purchase data
     * @return bool
     */
    protected function validatePurchaseData()
    {
        $arRuleSet = [
            'amount'   => 'required',
            'currency' => 'required',
        ];

        $obValidator = Validator::make($this->arPurchaseData, $arRuleSet);
        if ($obValidator->fails()) {
            $this->sResponseMessage = $obValidator->messages()->first();
            return false;
        }

        return true;
    }

    /**
     * Send purchase request to payment gateway
     */
    protected function sendPurchaseData()
    {
        $arPaymentData = (array) $this->obOrder->payment_data;
        $arPaymentData['request'] = $this->arPurchaseData;

        $this->obOrder->payment_data = $arPaymentData;
        $this->obOrder->save();
        try {
            $this->response = $this->obGateway->purchase($this->arPurchaseData);
        } catch (\Exception $obException) {
            $this->sResponseMessage = $obException->getMessage();
            return;
        }
    }

    /**
     * Process purchase response
     * @return void
     */
    protected function processPurchaseResponse()
    {
        if (empty($this->response)) {
            return;
        }
        if (!empty($this->response['formUrl'])) {
            $this->setWaitPaymentStatus();
            $arPaymentResponse['redirect_url'] = $this->response['formUrl'];
            $this->bIsRedirect = true;
        }else{
            $this->setSuccessStatus();
        }

        $arPaymentResponse = (array) $this->obOrder->payment_response;
        $arPaymentResponse['response'] = (array) $this->response;

        $this->obOrder->payment_response = $arPaymentResponse;
        $this->obOrder->payment_token = $this->response['orderId'];
        $this->obOrder->save();

    }
}
