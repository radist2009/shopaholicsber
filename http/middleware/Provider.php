<?php namespace Rubium\ShopaholicSber\Http\Middleware;

use Closure;

class Provider {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Exception
     */
	public function handle($request, Closure $next)
	{
		//Хэш коллбаков кажется неплохой идеей считать именно здесь
        \Log::info($request->all());
		die();

		if (!in_array($request->provider, $providers)) {
			throw new \Exception('Unauthorized action.');
		}

		return $next($request);
	}

}
