<?php namespace Rubium\ShopaholicSber\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('Rubium\ShopaholicSber\Http\Middleware\Provider');
    }

    public function callback()
    {
        var_dump('callback');
        return 200;
    }
}